package dam.android.vidal.tasca5_di_classwork;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    private ViewPager2 vpHorizontal;
    private final String[] titols = new String[]{"Tab1", "Tab2", "Tab3", "tab4"};
    private final int NUM_PAGES = 4;
    private LinearLayout dotsLayout;
    TextView next;

    public MainActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vpHorizontal = findViewById(R.id.vp_horizontal);

        FragmentStateAdapter pageAdapter = new MyPagerAdapter(this);
        vpHorizontal.setAdapter(pageAdapter);
        vpHorizontal.setPageTransformer(new HorizontalFlipTransformation());

        TabLayout tabLayout = findViewById(R.id.tab_layout);

        new TabLayoutMediator(tabLayout, vpHorizontal, (tab, position) -> {
            tab.setText(titols[position]);

            BadgeDrawable badgeDrawable = tab.getOrCreateBadge();
            badgeDrawable.setBackgroundColor(
                    ContextCompat.getColor(getApplicationContext(), R.color.teal_200)
            );
            badgeDrawable.setVisible(true);
            int badgeNumber = setBadgeNumber(position);
            badgeDrawable.setNumber(badgeNumber);

        }).attach();
        dotsLayout = findViewById(R.id.layoutDots);
        next = findViewById(R.id.tvNext);

        vpHorizontal.registerOnPageChangeCallback(viewPagerPageChangeListener);
    }

    public void nextPressed(View view){
        if (vpHorizontal.getCurrentItem() != titols.length){
            vpHorizontal.setCurrentItem(vpHorizontal.getCurrentItem() + 1);
        }
    }

    public void skipPressed(View view){
        vpHorizontal.setCurrentItem(0);
    }

    @Override
    public void onBackPressed() {
        if (vpHorizontal.getCurrentItem() == 0){
            super.onBackPressed();
        } else {
            vpHorizontal.setCurrentItem(vpHorizontal.getCurrentItem() - 1);
        }
    }

    private int setBadgeNumber(int position){
        switch (position){
            case 0: return 28;
            case 1: return 45;
            case 2: return 86;
            case 3: return 35;
            default: return 2;
        }
    }

    public void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[NUM_PAGES];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);


        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    ViewPager2.OnPageChangeCallback viewPagerPageChangeListener = new ViewPager2.OnPageChangeCallback() {

        @Override
        public void onPageSelected(int position) {
            if (position == NUM_PAGES - 1){

                next.setVisibility(View.INVISIBLE);
            }else {
                next.setVisibility(View.VISIBLE);
            }
            addBottomDots(position);
        }
    };
}

