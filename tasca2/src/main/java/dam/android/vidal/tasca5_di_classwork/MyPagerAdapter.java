package dam.android.vidal.tasca5_di_classwork;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import dam.android.vidal.tasca5_di_classwork.fragments.FourthFragment;
import dam.android.vidal.tasca5_di_classwork.fragments.OneFragment;
import dam.android.vidal.tasca5_di_classwork.fragments.SecondFragment;
import dam.android.vidal.tasca5_di_classwork.fragments.ThirdFragment;

public class MyPagerAdapter extends FragmentStateAdapter {

    private static final int NUM_PAGES = 4;
    public MyPagerAdapter(@NonNull FragmentActivity fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0: return new OneFragment();
            case 1: return new SecondFragment();
            case 2: return new ThirdFragment();
            default: return new FourthFragment();
        }
    }

    @Override
    public int getItemCount() {
        return NUM_PAGES;
    }
}
